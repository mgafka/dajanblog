<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.12.17
 * Time: 13:28
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadPostData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i=1; $i<=1000; $i++) {
            $post = new Post();
            $post->setTitle($faker->sentence(3));
            $post->setLead($faker->text(300));
            $post->setContent($faker->text(700));
            $post->setCreatedAt($faker->dateTimeThisMonth);
            $post->setCreatedBy("admin");

            $manager->persist($post);
        }

        $manager->flush();
    }
}