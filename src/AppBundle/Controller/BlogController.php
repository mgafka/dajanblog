<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Article;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       $qb =  $this->getDoctrine()
           ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Article', 'p')
            ->select('p')
            ->orderBy('p.id', 'DESC');
       $paginator = $this->get('knp_paginator');
       $pagination = $paginator->paginate(
         $qb,
         $request->query->get('page', 1),
           10
       );

        return $this->render('blog/index.html.twig', [
            'articles' => $pagination
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_show")
     */

    public function showAction(Article $article, Request $request)
    {
        $form = null;

        if ($user = $this->getUser()) {
            $comment = new Comment();
            $comment->setArticle($article);
            $comment->setUser($user);


            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                $this->addFlash('success', $this->get('translator')->trans("Comment has been succesfully added!"));

                return $this->redirectToRoute('article_show', array('id' => $article->getId()));
            }
        }

        return $this->render('blog/show.html.twig', [
            'article' => $article,
            'form' => is_null($form) ? $form : $form->createView()
        ]);

    }
}
