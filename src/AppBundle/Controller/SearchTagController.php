<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.01.18
 * Time: 15:48
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class SearchTagController extends Controller
{

    /**
     * @Route("/tag/{name}", name="tag_search")
     */
    public function tagAction($name)
    {

        $em = $this->getDoctrine()->getManager();

        $tags = $em ->getRepository('AppBundle:Tag')->findOneByName($name);



        return $this->render('blog/tag.html.twig', ['tag' => $tags]);

    }


}